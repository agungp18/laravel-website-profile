<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listemail extends Model
{
    protected $table = 'list_email';
    protected $fillable = ['email'];
}
