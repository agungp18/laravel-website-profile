<?php

namespace App\Http\Controllers;

use App\Galery;
use Illuminate\Http\Request;

class GaleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galery = Galery::paginate(5);
        return view('admin.galery.index', compact('galery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $galery =  new \App\Galery;
        $galery->name = $request->get('name');
        $galery->keterangan = $request->get('keterangan');
        $galery->tipe = $request->get('tipe');
        if ($request->file('picture')) {
            $file = $request->file('picture')->store('picture', 'public');
            $galery->picture =  $file;
        }
        $galery->save();
        return redirect('/admin/galery')->with('status', 'Data Berhasil Disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galery = Galery::find($id);
        return view('admin.galery.edit', compact('galery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $galery = Galery::find($id);
        $galery->name = $request->get('name');
        $galery->keterangan = $request->get('keterangan');
        $galery->tipe = $request->get('tipe');
        if ($request->picture) {
            if ($galery->picture && file_exists(storage_path('app/public/' . $galery->picture))) {
                \Storage::delete('public/' . $galery->picture);
                $file = $request->file('picture')->store('picture', 'public');
                $galery->picture = $file;
            }
        }
        $galery->save();
        return redirect('/admin/galery')->with('status', 'Galery Succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Galery::destroy($id);
        return redirect('/admin/galery')->with('status', ' Galery Succesfulyy deleted');
    }
}
