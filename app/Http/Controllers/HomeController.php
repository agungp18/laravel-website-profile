<?php

namespace App\Http\Controllers;

use App\Galery;
use App\News;
use App\Video;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $galery = Galery::count();
        $news = News::count();
        $video = Video::count();

        return view('admin.home', compact('galery', 'news', 'video'));
    }
}
