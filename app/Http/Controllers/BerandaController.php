<?php

namespace App\Http\Controllers;

use App\About;
use App\Video;
use App\Galery;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BerandaController extends Controller
{
    public function index()
    {
        $about = About::all();
        $video = Video::all();
        $contact = DB::table('contact')->get();
        $galery = Galery::all();
        $news = News::all();
        return view('user.beranda', compact('contact', 'about', 'video', 'galery', 'news'));
    }
}
