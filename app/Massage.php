<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Massage extends Model
{
    protected $table = 'massage';
    protected $fillable = [
        'name', 'email', 'subject', 'massage'
    ];
}
