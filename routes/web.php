<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BerandaController@index');

Route::get('/admin/news', 'NewsController@index');
Route::post('/admin/news', 'NewsController@store');
Route::delete('/admin/news/delete/{news}', 'NewsController@destroy');
Route::get('/admin/news/edit/{news}', 'NewsController@edit');
Route::patch('/admin/news/{news}/edit', 'NewsController@update');

Route::get('/admin/galery', 'GaleryController@index');
Route::post('/admin/galery', 'GaleryController@store');
Route::delete('/admin/galery/delete/{galery}', 'GaleryController@destroy');
Route::get('/admin/galery/edit/{galery}', 'GaleryController@edit');
Route::patch('/admin/galery/{galery}/edit', 'GaleryController@update');

Route::get('/admin/video', 'VideoController@index');
Route::post('/admin/video', 'VideoController@store');
Route::delete('/admin/video/delete/{video}', 'VideoController@destroy');
Route::get('/admin/video/edit/{video}', 'VideoController@edit');
Route::patch('/admin/video/{video}/edit', 'VideoController@update');

Route::get('/admin/contact', 'ContactController@index');
Route::delete('/admin/contact/delete/{contact}', 'ContactController@destroy');
Route::get('/admin/contact/edit/{contact}', 'ContactController@edit');
Route::patch('/admin/contact/{contact}/edit', 'ContactController@update');

Route::get('/admin/about', 'AboutController@index');
Route::delete('/admin/about/delete/{about}', 'AboutController@destroy');
Route::get('/admin/about/edit/{about}', 'AboutController@edit');
Route::patch('/admin/about/{about}/edit', 'AboutController@update');

Route::get('/admin/list-email', 'ListemailController@index');
Route::post('/subcribe', 'ListemailController@store');

Route::get('/admin/massage', 'MassageController@index');
Route::post('/massage', 'MassageController@store');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
