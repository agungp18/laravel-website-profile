@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-12">
        <h1 class="mt-3">Email List</h1>
    </div>

    <table class="table table-hover ">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Email</th>
                <th scope="col">Date</th>
            </tr>
        </thead>
        <tbody>
            @foreach($listEmail as $data)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$data->email}}</td>
                <td>{{$data->created_at}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection
