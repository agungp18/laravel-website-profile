@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="mt-3">Galery list</h1>
            <br>
            @if (session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
            @endif
        </div>
        <div class="col-6">
            <button type="button" class="btn btn-primary mt-3 float-lg-right" data-toggle="modal" data-target="#exampleModal">
                Insert
            </button>
        </div>

        <table class="table table-hover ">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Picture</th>
                    <th scope="col" width="300px">Keterangan</th>
                    <th scope="col">Tipe</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($galery as $data)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->name}}</td>
                    <td>
                        @if ($data->picture)
                        <img src="{{asset('storage/'.$data->picture)}}" width="70px">
                        @else
                        N/A
                        @endif
                    </td>
                    <td>{{$data->keterangan}}</td>
                    <td>{{$data->tipe}}</td>
                    <td>
                        <a href="/admin/galery/edit/{{$data->id}}" class="btn btn-success">Edit</a>
                        <form class="d-inline" action="/admin/galery/delete/{{$data->id}}" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Are you sure');">Delete</button>
                        </form>

                    </td>
                </tr>
                @endforeach
            </tbody>
            {!! $galery->links() !!}
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="/admin/galery" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class=" form-group">
                        <label for="picture">Picture</label>
                        <input type="file" class="form-control" id="picture" name="picture" required>
                    </div>
                    <div class="form-group">
                        <label for="tipe"></label>
                        <select name="tipe" class="form-control">
                            <option value="web">web</option>
                            <option value="app">app</option>
                            <option value="card">card</option>
                        </select>
                    </div>
                    <div class=" form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea class="form-control" name="keterangan"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary save" name="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
