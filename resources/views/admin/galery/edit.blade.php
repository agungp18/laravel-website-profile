@extends('layouts.main')

@section('content')
<div class="container">
    <form method="post" action="/admin/galery/{{$galery->id}}/edit" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            @if ($galery->picture)
            <img src="{{asset('storage/'.$galery->picture)}}" width="120px" />
            <br>
            @else
            No Picture For Galery
            @endif
            <input type="file" class="form-control" id="picture" name="picture">
            <small class="text-muted">Kosongkan jika tidak ingin mengubah Picture</small>

        </div>
        <div class=" form-group">
            <label for="keterangan">Keterangan</label>
            <textarea class="form-control" name="keterangan">{{$galery->keterangan}}</textarea>
        </div>
        <div class="form-group">
            <label for="tipe"></label>
            <select name="tipe" class="form-control">
                <?php if ($galery->tipe == 'web') : ?>
                    <option value="web" selected>web</option>
                    <option value="app">app</option>
                    <option value="card">card</option>
                <?php elseif ($galery->tipe == 'app') : ?>
                    <option value="web">web</option>
                    <option value="app" selected>app</option>
                    <option value="card">card</option>
                <?php else : ?>
                    <option value="web">web</option>
                    <option value="app">app</option>
                    <option value="card" selected>card</option>
                <?php endif; ?>
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Save</button>
        </div>
    </form>
</div>
@endsection
