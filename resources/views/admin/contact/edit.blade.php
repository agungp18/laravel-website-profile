@extends('layouts.main')

@section('content')
<div class="container">
    <form method="post" action="/admin/contact/{{$contact->id}}/edit" enctype="multipart/form-data">
        @method('patch')
        @csrf
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{$contact->email}}">
        </div>
        <div class=" form-group">
            <label for="Telephone">Telephone</label>
            <input type="text" class="form-control" name="telephone" id="telephone" value="{{$contact->telephone}}">
        </div>
        <div class=" form-group">
            <label for="alamat">Alamat</label>
            <textarea type="text" class="form-control" name="alamat" id="alamat">{{$contact->alamat}}</textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Save</button>
        </div>
    </form>
</div>
@endsection
