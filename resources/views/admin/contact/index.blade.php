@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-12">
        <h1 class="mt-3">Contact List</h1>
        <br>
        @if (session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
        @endif
    </div>

    <table class="table table-hover ">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col" width="300px">Email</th>
                <th scope="col" width="100px">Telephone</th>
                <th scope="col" width="300px">Alamat</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contact as $data)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$data->email}}</td>
                <td>{{$data->telephone}}</td>
                <td>{{$data->alamat}}</td>
                <td>
                    <a href="/admin/contact/edit/{{$data->id}}" class="btn btn-success">Edit</a>
                    <form class="d-inline" action="/admin/contact/delete/{{$data->id}}" method="post">
                        @method('delete')
                        @csrf
                        <button class="btn btn-danger" onclick="return confirm('Are you sure');">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
@endsection
