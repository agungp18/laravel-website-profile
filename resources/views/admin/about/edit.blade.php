@extends('layouts.main')

@section('content')
<div class="container">
    <form method="post" action="/admin/about/{{$about->id}}/edit">
        @method('patch')
        @csrf
        <div class=" form-group">
            <label for="Sejarah">Sejarah</label>
            <textarea class="form-control" name="sejarah" rows="10">{{$about->sejarah}}</textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Save</button>
        </div>
    </form>
</div>
@endsection
