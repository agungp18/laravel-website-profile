@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1 class="mt-3">Video</h1>
            <br>
            @if (session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
            @endif
        </div>
        <div class="col-6">
            <button type="button" class="btn btn-primary mt-3 float-lg-right" data-toggle="modal" data-target="#exampleModal">
                Insert
            </button>
        </div>

        <table class="table table-hover ">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Video</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($video as $data)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$data->name}}</td>
                    <td>
                        <iframe width="300" height="300" src="http://www.youtube.com/embed/{{$data->video}}" frameborder="0" allowfullscreen>
                        </iframe>
                    </td>
                    <td>
                        <a href="/admin/video/edit/{{$data->id}}" class="btn btn-success">Edit</a>
                        <form class="d-inline" action="/admin/video/delete/{{$data->id}}" method="post">
                            @method('delete')
                            @csrf
                            <button class="btn btn-danger" onclick="return confirm('Are you sure');">Delete</button>
                        </form>

                    </td>
                </tr>
                @endforeach
            </tbody>
            {!! $video->links() !!}
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="/admin/video">
                    @csrf
                    <div class=" form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class=" form-group">
                        <label for="video">Video</label>
                        <input type="text" class="form-control" id="video" name="video" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary save" name="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
