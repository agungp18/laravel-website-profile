@extends('layouts.main')

@section('content')
<div class="container">
    <form method="post" action="/admin/video/{{$video->id}}/edit">
        @method('patch')
        @csrf
        <div class=" form-group">
            <label for="nama">Name</label>
            <input class="form-control" name="name" value="{{$video->name}}">
        </div>
        <div class="form-group">
            <iframe width="560" height="315" src="http://www.youtube.com/embed/{{$video->video}}" frameborder="0" allowfullscreen>
            </iframe>
            <input type="text" class="form-control" id="video" name="video" value="https://www.youtube.com/watch?v={{$video->video}}">

        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Save</button>
        </div>
    </form>
</div>
@endsection
