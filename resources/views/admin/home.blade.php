@extends('layouts.main')

@section('content')

<div class="panel-header bg-primary-gradient">
    <div class="page-inner py-5">
        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
            <div>
                <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
            </div>
        </div>
    </div>
</div>
<div class="row mt-5">
    <div class="col-6 col-sm-4 col-lg-4">
        <div class="card">
            <div class="card-body p-3 text-center">
                <div class="h1 m-0">{{$video}} </div>
                <div class="text-muted mb-3">Video</div>
            </div>
        </div>
    </div>
    <div class="col-6 col-sm-4 col-lg-4">
        <div class="card">
            <div class="card-body p-3 text-center">
                <div class="h1 m-0">{{$news}}</div>
                <div class="text-muted mb-3">News</div>
            </div>
        </div>
    </div>
    <div class="col-6 col-sm-4 col-lg-4">
        <div class="card">
            <div class="card-body p-3 text-center">
                <div class="h1 m-0">{{$galery}}</div>
                <div class="text-muted mb-3">Galery</div>
            </div>
        </div>
    </div>
</div>
@endsection
