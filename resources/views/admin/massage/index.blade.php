@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-12">
        <h1 class="mt-3">Massage List</h1>
    </div>

    <table class="table table-hover ">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Subject</th>
                <th scope="col">Massage</th>
            </tr>
        </thead>
        <tbody>
            @foreach($massage as $data)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$data->name}}</td>
                <td>{{$data->email}}</td>
                <td>{{$data->subject}}</td>
                <td>{{$data->massage}}</td>
            </tr>
            @endforeach
        </tbody>
        {!! $massage->links() !!}
    </table>
</div>
</div>
@endsection
