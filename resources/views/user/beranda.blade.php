<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>SIBERLAB</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link href="{{asset('img/favicon.png')}}" rel="icon">
    <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">
    <style>
        .sosmed {
            padding: 0;
            margin: 0;
            position: fixed;
            right: -119px;
            top: 20%;
            width: 200px
        }

        .sosmed input#hideshare,
        .sosmed input#openall {
            display: none
        }

        .sosmed ul,
        .sosmed ul li {
            list-style: none;
            list-style-type: none
        }

        .sticky li.openall {
            margin: 0;
            padding: 0
        }

        .sticky li.share {
            background-color: #333;
            color: #efefef;
            height: 43px;
            padding: 0;
            margin: 0 0 1px 0;
            -webkit-transition: all 0.25s ease-in-out;
            -moz-transition: all 0.25s ease-in-out;
            -o-transition: all 0.25s ease-in-out;
            transition: all 0.25s ease-in-out;
            cursor: pointer
        }

        .sticky li.share:hover {
            margin-left: -10px;
            -webkit-transform: translateX(-115px);
            -moz-transform: translateX(-115px);
            -o-transform: translateX(-115px);
            -ms-transform: translateX(-115px);
            transform: translateX(-115px)
        }

        .sticky li.share i {
            float: left;
            margin: 11px;
            margin-right: 15px;
            color: white;
            font-size: 20px
        }

        .sticky li.share b {
            padding: 0;
            margin: 0;
            text-transform: uppercase;
            line-height: 43px
        }

        .sticky li a {
            text-decoration: none;
            color: white
        }

        .sosmed li.facebook {
            background: #3b5999
        }

        .sosmed li.twitter {
            background: #55acee
        }

        .sosmed li.google {
            background: #dd4b39
        }

        input:checked#hideshare~li.share,
        input:checked#hideshare~li.openall {
            display: none
        }

        label span.show,
        .sosmed li.openall label {
            position: relative;
            cursor: pointer;
            display: block;
            width: 43px;
            font-size: 20px
        }

        .sosmed li.openall label {
            text-align: center;
            background: #bbb;
            color: white
        }

        input:checked#hideshare~label span.show {
            padding: 5px;
            text-align: center;
        }

        label span.show:after {
            content: 'f105';
            font-family: FontAwesome;
        }

        input:checked#hideshare~label span.show:after {
            content: 'f104';
        }
    </style>
    </style>

    <!-- Bootstrap CSS File -->
    <link href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="{{asset('lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/ionicons/css/ionicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/lightbox/css/lightbox.min.css')}}" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

</head>

<body>
    <!--==========================
  Header
  ============================-->
    <header id="header">

        <div id="topbar">
            <div class="container">
                <div class="social-links">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="logo float-left">
                <!-- Uncomment below if you prefer to use an image logo -->
                <h1 class="text-light"><a href="#intro" class="scrollto"><span>SiberLab</span></a></h1>
                <!-- <a href="#header" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a> -->
            </div>

            <nav class="main-nav float-right d-none d-lg-block">
                <ul>
                    <li class="active"><a href="#intro">Home</a></li>
                    <li><a href="#about">About Us</a></li>
                    <li><a href="#news">News</a></li>
                    <li><a href="#portfolio">Galery</a></li>
                    <li><a href="#video">video</a></li>
                    <li><a href="#footer">Contact Us</a></li>
                </ul>
            </nav><!-- .main-nav -->

        </div>
    </header><!-- #header -->

    <!--==========================
    Intro Section
  ============================-->
    <section id="intro" class="clearfix">
        <div class="container d-flex h-100">
            <div class="row justify-content-center align-self-center">
                <div class="col-md-6 intro-info order-md-first order-last">
                    <h2>SIBERLAB Solutions<br>for Your <span>Business!</span></h2>
                    <div>
                        <a href="#about" class="btn-get-started scrollto">Get Started</a>
                    </div>
                </div>

                <div class="col-md-6 intro-img order-md-last order-first">
                    <img src="img/intro-img.svg" alt="" class="img-fluid">
                </div>
            </div>

        </div>
    </section><!-- #intro -->

    <main id="main">

        <!--==========================
      About Us Section
    ============================-->
        <section id="about">

            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-6">
                        <div class="about-img">
                            <img src="img/about-img.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="about-content">
                            <h2>About Us</h2>
                            @foreach($about as $about)
                            <p>{{$about->sejarah}}</p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </section><!-- #about -->


        <!--==========================
      Services Section
    ============================-->
        <section id="news" class="section-bg">
            <div class="container">


                <header class="section-header">
                    <h3>News</h3>
                </header>

                <div class="row">
                    @foreach($news as $news)

                    <div class="col-md-4 wow bounceInUp" data-wow-duration="1.4s">
                        <div class="card-deck">
                            <div class="card">
                                <img src="{{asset('storage/'.$news->picture)}}" class=" card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title">{{$news->news}}</h5>
                                    <p class="card-text">{{$news->keterangan}}</p>
                                    <p class="card-text"><small class="text-muted">{{$news->date}}</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
        </section><!-- #services -->
        <section id="portfolio" class="section-bg">
            <div class="container">

                <header class="section-header">
                    <h3 class="section-title">Galery</h3>
                </header>

                <div class="row">
                    <div class="col-lg-12">
                        <ul id="portfolio-flters">
                            <li data-filter="*" class="filter-active">All</li>
                            <li data-filter=".filter-app">App</li>
                            <li data-filter=".filter-card">Card</li>
                            <li data-filter=".filter-web">Web</li>
                        </ul>
                    </div>
                </div>

                <div class="row portfolio-container">
                    @foreach($galery as $galery)
                    <div class="col-lg-4 col-md-6 portfolio-item filter-{{$galery->tipe}}">
                        <div class="portfolio-wrap">
                            <img src="{{asset('storage/'.$galery->picture)}}" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4><a href="#">{{$galery->name}}</a></h4>
                                <p>{{$galery->keterangan}}</p>
                                <div>
                                    <a href="{{asset('storage/'.$galery->picture)}}" data-lightbox="portfolio" data-title="{{$galery->name}}" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                                    <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
        </section><!-- #portfolio -->

        <!--==========================
      Clients Section
    ============================-->
        <!--==========================
      Team Section
    ============================-->
        <section id="video" class="section-bg">
            <div class="container">
                <div class="section-header">
                    <h3>Video</h3>
                </div>
                <div class="row">
                    @foreach($video as $video)
                    <div class="col-lg-4 wow fadeInUp">
                        <div class="video">
                            <iframe width="300" height="300" src="http://www.youtube.com/embed/{{$video->video}}" frameborder="0" allowfullscreen>
                            </iframe>
                        </div>
                    </div>
                    @endforeach
                </div>

            </div>
        </section>
    </main>

    <!--==========================
    Footer
  ============================-->
    <footer id="footer" class="section-bg">
        <div class="footer-top">
            <div class="container">

                <div class="row">

                    <div class="col-lg-6">

                        <div class="row">

                            <div class="col-sm-6">

                                <div class="footer-info">
                                    <h3>Siberlab</h3>
                                    <p>Salah satu perusahaan yang bergerak didalam bidang IT yang akan selalau membuat inovasi dan terobosan
                                        dalam teknologi dan berperan baik bagi negara, bangsa dan tanah air </p>
                                </div>

                                <div class="footer-newsletter">
                                    <h4>Our Newsletter</h4>
                                    <p>Jangan lupa ikuti website ini agar bisa mendapatkan pemberitahuan secara intessif</p>
                                    <form action="/subcribe" method="post">
                                        @csrf
                                        <input type="email" name="email"><input type="submit" value="Subscribe">
                                    </form>
                                </div>

                            </div>

                            <div class="col-sm-6">
                                <div class="footer-links">
                                    <h4>Useful Links</h4>
                                    <ul>
                                        <li><a href="#about">About us</a></li>
                                        <li><a href="#news">News</a></li>
                                        <li><a href="#portofolio">Galery</a></li>
                                        <li><a href="#video">Video</a></li>
                                    </ul>
                                </div>

                                <div class="footer-links">
                                    <h4>Contact Us</h4>
                                    @foreach($contact as $contact)
                                    <p>
                                        {{$contact->alamat}}<br>
                                        <strong>Phone: </strong> {{$contact->telephone}}<br>
                                        <strong>Email: </strong> {{$contact->email}}<br>
                                    </p>
                                    @endforeach
                                </div>

                                <div class="social-links">
                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="form">

                            <h4>Send us a message</h4>
                            <form method="post" action="/massage">
                                @csrf
                                <div class=" form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                                <div class=" form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" required>
                                </div>
                                <div class=" form-group">
                                    <label for="subject">Subject</label>
                                    <input type="text" class="form-control" id="subject" name="subject" required>
                                </div>
                                <div class=" form-group">
                                    <label for="massage">Massage</label>
                                    <textarea type="text" class="form-control" id="massage" name="massage" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary save" name="submit">send</button>
                        </div>
                        </form>
                    </div>

                </div>



            </div>

        </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong>SIBERLAB</strong>. All Rights Reserved
            </div>
            <div class="credits">
                Designed by SIBERLAB
            </div>
        </div>
    </footer><!-- #footer -->

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
    <div class="sosmed">
        <ul class="sticky">
            <input id='hideshare' type="checkbox" />
            <li class="share facebook"><span><a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i> <b>Facebook</b></a></span></li>
            <li class="share twitter"><span><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i> <b>Twitter</b></a></span></li>
            <li class="share whatsapp"><span><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i> <b>WhatsApp</b></a></span></li>
            <li class="share google"><span><a href="#"><i class="fa fa-google-plus-square" aria-hidden="true"></i> <b>Google+</b></a></span></li>
        </ul>
    </div>
    <!-- Uncomment below i you want to use a preloader -->
    <!-- <div id="preloader"></div> -->

    <!-- JavaScript Libraries -->
    <script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('lib/jquery/jquery-migrate.min.js')}}"></script>
    <script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('lib/easing/easing.min.js')}}"></script>
    <script src="{{asset('lib/mobile-nav/mobile-nav.js')}}"></script>
    <script src="{{asset('lib/wow/wow.min.js')}}"></script>
    <script src="{{asset('lib/waypoints/waypoints.min.js')}}"></script>
    <script src="{{asset('lib/counterup/counterup.min.js')}}"></script>
    <script src="{{asset('lib/owlcarousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('lib/isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('lib/lightbox/js/lightbox.min.js')}}"></script>
    <!-- Contact Form JavaScript File -->
    <script src="{{asset('contactform/contactform.js')}}"></script>

    <!-- Template Main Javascript File -->
    <script src="{{asset('js/main.js')}}"></script>

</body>

</html>
